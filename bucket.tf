resource "scaleway_object_bucket" "terraform-bucket" {
  name = "terraform-s3-bucket"
  project_id = scaleway_account_project.terraform_bucket.id

  lifecycle_rule {
    enabled = true
    expiration {
      days = "7"
    }
  }
}

#resource "scaleway_object_bucket_policy" "terraform-bucket-policy" {
#    bucket = scaleway_object_bucket.terraform-bucket.name
#    project_id = scaleway_account_project.terraform_bucket.id
#    policy = jsonencode(
#    {
#        Id = "MyPolicy"
#        Statement = [
#        {
#            Action = [
#                "s3:*",
#            ]
#           Effect = "Allow"
#           Principal = {
#               SCW = "user_id:xxxxxxxxxxxxxxxx"
#            }
#           Resource  = [
#              "${scaleway_object_bucket.terraform-bucket.name}",
#              "${scaleway_object_bucket.terraform-bucket.name}/*"
#            ]
#           Sid = "bucket user policy"
#        },
#        {
#            Action = [
#                "s3:PutObject",
#            ]
#           Effect = "Allow"
#           Principal = {
#               SCW = "application_id:${scaleway_iam_application.main.id}"
#            }
#           Resource  = [
#              "${scaleway_object_bucket.terraform-bucket.name}/*"
#            ]
#           Sid = "bucket put policy"
#        },
#        {
#            Action = [
#                "s3:GetObject",
#            ]
#           Effect = "Allow"
#           Principal = {
#               SCW = "application_id:${scaleway_iam_application.main.id}"
#            }
#           Resource  = [
#              "${scaleway_object_bucket.terraform-bucket.name}/*"
#            ]
#           Sid = "bucket get policy"
#        },
#    ]
#    Version = "2023-04-17"
#    }
#    )
#}
