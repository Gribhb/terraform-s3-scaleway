resource scaleway_iam_policy "s3_write_object" {
  name = "s3 write object"
  description = "gives app write to object storage in project"
  application_id = scaleway_iam_application.s3_bucket_terraform.id
  rule {
    project_ids = [scaleway_account_project.terraform_bucket.id]
    permission_set_names = ["ObjectStorageObjectsWrite"]
  }
}
