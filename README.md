# Terraform S3 Scaleway

## Usage

The first make sure to have an Scaleway account, and have credentials configured.

Then clone this repository 

```
git clone https://gitlab.com/Gribhb/terraform-s3-scaleway.git
```

Finally, deploy :

```
terraform init
terraform plan
terraform appy
```

My related blog post : https://blog.antoinemayer.fr/2023/05/25/deployer-un-bucket-s3-sur-scaleway-avec-terraform/


## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_scaleway"></a> [scaleway](#provider\_scaleway) | 2.19.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [scaleway_account_project.terraform_bucket](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/account_project) | resource |
| [scaleway_iam_application.s3_bucket_terraform](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/iam_application) | resource |
| [scaleway_iam_policy.s3_write_object](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/iam_policy) | resource |
| [scaleway_object_bucket.terraform-bucket](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/object_bucket) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_region"></a> [region](#input\_region) | n/a | `string` | `"fr-par"` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | n/a | `string` | `"fr-par-1"` | no |

## Outputs

No outputs.
